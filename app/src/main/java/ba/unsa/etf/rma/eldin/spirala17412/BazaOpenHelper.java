package ba.unsa.etf.rma.eldin.spirala17412;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static ba.unsa.etf.rma.eldin.spirala17412.KategorijeAkt.mojaBaza;

public class BazaOpenHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "mojaBaza.db";
    public static final String DATABASE_TABLE1 = "Kategorija";
    public static final String KATEGORIJA_ID = "_id";
    public static final String KATEGORIJA_NAZIV = "naziv";
    public static final String DATABASE_TABLE2 = "Knjiga";
    public static final String KNJIGA_ID = "_id";
    public static final String KNJIGA_NAZIV = "naziv";
    public static final String KNJIGA_OPIS = "opis";
    public static final String KNJIGA_DATUMOBJAVLJIVANJA = "datumObjavljivanja";
    public static final String KNJIGA_BROJSTRANICA = "brojStranica";
    public static final String KNJIGA_IDWEBSERVIS = "idWebServis";
    public static final String KNJIGA_IDKATEGORIJE = "idkategorije";
    public static final String KNJIGA_SLIKA = "slika";
    public static final String KNJIGA_PREGLEDANA = "pregledana";
    public static final String DATABASE_TABLE3 = "Autor";
    public static final String AUTOR_ID = "_id";
    public static final String AUTOR_IME = "ime";
    public static final String DATABASE_TABLE4 = "Autorstvo";
    public static final String AUTORSTVO_ID = "_id";
    public static final String AUTORSTVO_IDAUTORA = "idautora";
    public static final String AUTORSTVO_IDKNJIGE = "idknjige";
    public static final int DATABASE_VERSION = 1;

    private static final String DATABASE_CREATE1 = "create table " + DATABASE_TABLE1 + " (" + KATEGORIJA_ID + " integer primary key autoincrement, " + KATEGORIJA_NAZIV + " text not null);";
    private static final String DATABASE_CREATE2 = "create table " + DATABASE_TABLE2 + " (" + KNJIGA_ID + " integer primary key autoincrement, " + KNJIGA_NAZIV + " text not null, " + KNJIGA_OPIS + " text, " + KNJIGA_DATUMOBJAVLJIVANJA + " text, " + KNJIGA_BROJSTRANICA + " integer, " + KNJIGA_IDWEBSERVIS + " text, " + KNJIGA_IDKATEGORIJE + " integer not null, " + KNJIGA_SLIKA + " text, " + KNJIGA_PREGLEDANA + " integer not null);";
    private static final String DATABASE_CREATE3 = "create table " + DATABASE_TABLE3 + " (" + AUTOR_ID + " integer primary key autoincrement, " + AUTOR_IME + " text not null);";
    private static final String DATABASE_CREATE4 = "create table " + DATABASE_TABLE4 + " (" + AUTORSTVO_ID + " integer primary key autoincrement, " + AUTORSTVO_IDAUTORA + " integer not null, " + AUTORSTVO_IDKNJIGE + " integer not null);";

    public BazaOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DATABASE_CREATE1);
        db.execSQL(DATABASE_CREATE2);
        db.execSQL(DATABASE_CREATE3);
        db.execSQL(DATABASE_CREATE4);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE1);
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE2);
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE3);
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE4);

        onCreate(db);
    }

    public long dodajKategoriju(String naziv) {
        int idKategorije = -1;

        String[] koloneRezultat = new String[]{KATEGORIJA_ID, KATEGORIJA_NAZIV};

        SQLiteDatabase db = mojaBaza.getWritableDatabase();
        Cursor cursor = db.query(BazaOpenHelper.DATABASE_TABLE1, koloneRezultat, null, null, null, null, null);
        int index_kolone_id = cursor.getColumnIndex(BazaOpenHelper.KATEGORIJA_ID);
        int index_kolone_naziv = cursor.getColumnIndex(BazaOpenHelper.KATEGORIJA_NAZIV);
        while (cursor.moveToNext()) {
            if (naziv.equals(cursor.getString(index_kolone_naziv)))
                return idKategorije;
        }
        cursor.close();

        ContentValues noveVrijednosti = new ContentValues();
        noveVrijednosti.put(KATEGORIJA_NAZIV, naziv);
        db.insert(BazaOpenHelper.DATABASE_TABLE1, null, noveVrijednosti);
        db.close();

        db = mojaBaza.getReadableDatabase();
        cursor = db.query(BazaOpenHelper.DATABASE_TABLE1, koloneRezultat, null, null, null, null, null);

        while (cursor.moveToNext()) {
            if (naziv.equals(cursor.getString(index_kolone_naziv))) {
                idKategorije = cursor.getInt(index_kolone_id);
                break;
            }
        }
        cursor.close();
        db.close();

        return idKategorije;
    }

    public long dodajKnjigu(Knjiga knjiga) {
        int idKnjige = -1;

        String[] koloneRezultatKategorije = new String[]{KATEGORIJA_ID, KATEGORIJA_NAZIV};
        String[] koloneRezultatKnjiga = new String[]{KNJIGA_ID, KNJIGA_NAZIV, KNJIGA_OPIS, KNJIGA_DATUMOBJAVLJIVANJA, KNJIGA_BROJSTRANICA, KNJIGA_IDWEBSERVIS, KNJIGA_IDKATEGORIJE, KNJIGA_SLIKA, KNJIGA_PREGLEDANA};
        String[] koloneRezultatAutor = new String[]{AUTOR_ID, AUTOR_IME};

        SQLiteDatabase db = mojaBaza.getWritableDatabase();
        Cursor cursor = db.query(DATABASE_TABLE2, koloneRezultatKnjiga, null, null, null, null, null);
        int index_kolone_naziv = cursor.getColumnIndex(KNJIGA_NAZIV);
        while (cursor.moveToNext()) {
            if (knjiga.getNaziv().equals(cursor.getString(index_kolone_naziv)))
                return idKnjige;
        }
        cursor.close();

        String naziv = null;
        if(knjiga.getNaziv() != null)
            naziv = knjiga.getNaziv();
        String opis = null;
        if(knjiga.getOpis() != null)
            opis = knjiga.getOpis();
        String datumObjavljivanja = null;
        if(knjiga.getDatumObjavljivanja() != null)
            datumObjavljivanja = knjiga.getDatumObjavljivanja();
        int brojStranica = 0;
        if(knjiga.getBrojStranica() != 0)
            brojStranica = knjiga.getBrojStranica();
        String idWebServisa = null;
        if(knjiga.getId() != null)
            idWebServisa = knjiga.getId();
        String imeKategorije = null;
        if(knjiga.getNazivKategorije() != null)
            imeKategorije = knjiga.getNazivKategorije();
        int idKat = -1;
        String slika = null;
        if(knjiga.getSlika() != null)
            slika = knjiga.getSlika().toString();
        int pregledana = 0;
        if(knjiga.getBoju())
            pregledana = 1;

        cursor = db.query(DATABASE_TABLE1, koloneRezultatKategorije, null, null, null, null, null);
        int index_kolone_id = cursor.getColumnIndex(KATEGORIJA_ID);
        int index_kolone_naziv_kat = cursor.getColumnIndex(KATEGORIJA_NAZIV);
        while (cursor.moveToNext()) {
            if (imeKategorije.equals(cursor.getString(index_kolone_naziv_kat))) {
                idKat = cursor.getInt(index_kolone_id);
                break;
            }
        }
        cursor.close();

        ContentValues noveVrijednosti = new ContentValues();
        noveVrijednosti.put(KNJIGA_NAZIV, naziv);
        noveVrijednosti.put(KNJIGA_OPIS, opis);
        noveVrijednosti.put(KNJIGA_DATUMOBJAVLJIVANJA, datumObjavljivanja);
        noveVrijednosti.put(KNJIGA_BROJSTRANICA, brojStranica);
        noveVrijednosti.put(KNJIGA_IDWEBSERVIS, idWebServisa);
        noveVrijednosti.put(KNJIGA_IDKATEGORIJE, idKat);
        noveVrijednosti.put(KNJIGA_SLIKA, slika);
        noveVrijednosti.put(KNJIGA_PREGLEDANA, pregledana);
        db.insert(DATABASE_TABLE2, null, noveVrijednosti);

        cursor = db.query(DATABASE_TABLE3, koloneRezultatAutor, null, null, null, null, null);
        int index_kolone_ime_autor = cursor.getColumnIndex(AUTOR_IME);
        ArrayList<Autor> autori = knjiga.getAutori();
        String autor = null;
        if(autori == null)
            autor = knjiga.getImeAutora();

        if(autor == null) {
            for (int i = 0; i < autori.size(); i++) {
                boolean ima = false;
                cursor.moveToFirst();
                while (cursor.moveToNext()) {
                    if(autori.get(i).getImeiPrezime().equals(cursor.getString(index_kolone_ime_autor))) {
                        ima = true;
                        break;
                    }
                }
                if(!ima) {
                    noveVrijednosti = new ContentValues();
                    noveVrijednosti.put(AUTOR_IME, autori.get(i).getImeiPrezime());
                    db.insert(DATABASE_TABLE3, null, noveVrijednosti);
                }
            }
        }
        else {
            boolean ima = false;
            while (cursor.moveToNext()) {
                if(autor.equals(cursor.getString(index_kolone_ime_autor))) {
                    ima = true;
                    break;
                }
            }
            if(!ima) {
                noveVrijednosti = new ContentValues();
                noveVrijednosti.put(AUTOR_IME, autor);
                db.insert(DATABASE_TABLE3, null, noveVrijednosti);
            }
        }
        cursor.close();
        db.close();

        db = mojaBaza.getWritableDatabase();
        cursor = db.query(DATABASE_TABLE2, koloneRezultatKnjiga, null, null, null, null, null);
        index_kolone_id = cursor.getColumnIndex(KNJIGA_ID);
        index_kolone_naziv = cursor.getColumnIndex(KNJIGA_NAZIV);
        while (cursor.moveToNext()) {
            if(knjiga.getNaziv().equals(cursor.getString(index_kolone_naziv))) {
                idKnjige = cursor.getInt(index_kolone_id);
                break;
            }
        }
        cursor.close();

        cursor = db.query(DATABASE_TABLE3, koloneRezultatAutor, null, null, null, null, null);
        index_kolone_id = cursor.getColumnIndex(AUTOR_ID);
        index_kolone_ime_autor = cursor.getColumnIndex(AUTOR_IME);
        while (cursor.moveToNext()) {
            int id = cursor.getInt(index_kolone_id);
            String ime = cursor.getString(index_kolone_ime_autor);
            if(autor == null) {
                for(int i=0; i<autori.size(); i++) {
                    if(autori.get(i).getImeiPrezime().equals(ime)) {
                        ContentValues novoAutorstvo = new ContentValues();
                        novoAutorstvo.put(AUTORSTVO_IDAUTORA, id);
                        novoAutorstvo.put(AUTORSTVO_IDKNJIGE, idKnjige);
                        db.insert(DATABASE_TABLE4, null, novoAutorstvo);
                        break;
                    }
                }
            }
            else {
                if(autor.equals(ime)) {
                    ContentValues novoAutorstvo = new ContentValues();
                    novoAutorstvo.put(AUTORSTVO_IDAUTORA, id);
                    novoAutorstvo.put(AUTORSTVO_IDKNJIGE, idKnjige);
                    db.insert(DATABASE_TABLE4, null, novoAutorstvo);
                    break;
                }
            }
        }
        cursor.close();
        db.close();

        return idKnjige;
    }

    public ArrayList<Knjiga> knjigeKategorije(long idKategorije) {
        ArrayList<Knjiga> knjigice = new ArrayList<>();

        String[] koloneRezultatKnjiga = new String[]{KNJIGA_ID, KNJIGA_NAZIV, KNJIGA_OPIS, KNJIGA_DATUMOBJAVLJIVANJA, KNJIGA_BROJSTRANICA, KNJIGA_IDWEBSERVIS, KNJIGA_IDKATEGORIJE, KNJIGA_SLIKA, KNJIGA_PREGLEDANA};
        String where = KNJIGA_IDKATEGORIJE + "=" + idKategorije;

        SQLiteDatabase db = mojaBaza.getReadableDatabase();
        Cursor cursor = db.query(DATABASE_TABLE2, koloneRezultatKnjiga, where, null, null, null, null);
        int index_kolone_id = cursor.getColumnIndex(KNJIGA_ID);
        int index_kolone_naziv = cursor.getColumnIndex(KNJIGA_NAZIV);
        int index_kolone_opis = cursor.getColumnIndex(KNJIGA_OPIS);
        int index_kolone_datum = cursor.getColumnIndex(KNJIGA_DATUMOBJAVLJIVANJA);
        int index_kolone_brojStranica = cursor.getColumnIndex(KNJIGA_BROJSTRANICA);
        int index_kolone_slika = cursor.getColumnIndex(KNJIGA_SLIKA);
        int index_kolone_pregledan = cursor.getColumnIndex(KNJIGA_PREGLEDANA);
        while (cursor.moveToNext()) {
            String id = cursor.getString(index_kolone_id);
            String naziv = cursor.getString(index_kolone_naziv);
            String opis = cursor.getString(index_kolone_opis);
            String datum = cursor.getString(index_kolone_datum);
            int brojStranica = cursor.getInt(index_kolone_brojStranica);
            URL slika = null;
            try {
                slika = new URL(cursor.getString(index_kolone_slika));
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            boolean pregledan = false;
            if(cursor.getInt(index_kolone_pregledan) == 1)
                pregledan = true;

            ArrayList<Autor> autori = new ArrayList<>();
            List<Integer> idSvihAutora = new ArrayList<Integer>();
            String[] koloneRezultatAutorstvo = new String[]{AUTORSTVO_ID, AUTORSTVO_IDAUTORA, AUTORSTVO_IDKNJIGE};
            String where2 = AUTORSTVO_IDKNJIGE + "=" + id;
            Cursor cursor2 = db.query(DATABASE_TABLE4, koloneRezultatAutorstvo, where2, null, null, null, null);
            int index_kolone_idAutora = cursor2.getColumnIndex(AUTORSTVO_IDAUTORA);
            while(cursor2.moveToNext())
                idSvihAutora.add(cursor2.getInt(index_kolone_idAutora));
            cursor2.close();

            String[] koloneRezultatAutor = new String[]{AUTOR_ID, AUTOR_IME};
            Cursor cursor3 = db.query(DATABASE_TABLE3, koloneRezultatAutor, null, null, null, null, null);
            int index_kolone_idAutor = cursor3.getColumnIndex(AUTOR_ID);
            int index_kolone_imeAutor = cursor3.getColumnIndex(AUTOR_IME);
            while(cursor3.moveToNext()) {
                int noviIdAutora = cursor3.getInt(index_kolone_idAutor);
                if(idSvihAutora.contains(noviIdAutora))
                    autori.add(new Autor(cursor3.getString(index_kolone_imeAutor), id));
            }
            cursor3.close();

            knjigice.add(new Knjiga(id, naziv, autori, opis, datum, slika, brojStranica));
            knjigice.get(knjigice.size()-1).setBoju(pregledan);
        }
        cursor.close();
        db.close();

        return knjigice;
    }

    public ArrayList<Knjiga> knjigeAutora(long idAutora) {
        ArrayList<Knjiga> knjigice = new ArrayList<>();

        SQLiteDatabase db = mojaBaza.getReadableDatabase();
        List<Integer> idSvihKnjiga = new ArrayList<Integer>();
        String[] koloneRezultatAutorstvo = new String[]{AUTORSTVO_ID, AUTORSTVO_IDAUTORA, AUTORSTVO_IDKNJIGE};
        String where2 = AUTORSTVO_IDAUTORA + "=" + idAutora;
        Cursor cursor2 = db.query(DATABASE_TABLE4, koloneRezultatAutorstvo, where2, null, null, null, null);
        int index_kolone_idKnjiga = cursor2.getColumnIndex(AUTORSTVO_IDKNJIGE);
        while(cursor2.moveToNext())
            idSvihKnjiga.add(cursor2.getInt(index_kolone_idKnjiga));
        cursor2.close();

        String imeAutora = null;
        String[] koloneRezultatAutor = new String[]{AUTOR_ID, AUTOR_IME};
        String where3 = AUTOR_ID + "=" + idAutora;
        Cursor cursor3 = db.query(DATABASE_TABLE3, koloneRezultatAutor, where3, null, null, null, null);
        int index_kolone_imeAutor = cursor3.getColumnIndex(AUTOR_IME);
        while(cursor3.moveToNext()) {
            imeAutora = cursor3.getString(index_kolone_imeAutor);
            break;
        }
        cursor3.close();

        String[] koloneRezultatKnjiga = new String[]{KNJIGA_ID, KNJIGA_NAZIV, KNJIGA_OPIS, KNJIGA_DATUMOBJAVLJIVANJA, KNJIGA_BROJSTRANICA, KNJIGA_IDWEBSERVIS, KNJIGA_IDKATEGORIJE, KNJIGA_SLIKA, KNJIGA_PREGLEDANA};
        Cursor cursor = db.query(DATABASE_TABLE2, koloneRezultatKnjiga, null, null, null, null, null);
        int index_kolone_id = cursor.getColumnIndex(KNJIGA_ID);
        int index_kolone_naziv = cursor.getColumnIndex(KNJIGA_NAZIV);
        int index_kolone_opis = cursor.getColumnIndex(KNJIGA_OPIS);
        int index_kolone_datum = cursor.getColumnIndex(KNJIGA_DATUMOBJAVLJIVANJA);
        int index_kolone_brojStranica = cursor.getColumnIndex(KNJIGA_BROJSTRANICA);
        int index_kolone_slika = cursor.getColumnIndex(KNJIGA_SLIKA);
        int index_kolone_pregledan = cursor.getColumnIndex(KNJIGA_PREGLEDANA);
        while (cursor.moveToNext()) {
            int id = cursor.getInt(index_kolone_id);
            String naziv = cursor.getString(index_kolone_naziv);
            String opis = cursor.getString(index_kolone_opis);
            String datum = cursor.getString(index_kolone_datum);
            int brojStranica = cursor.getInt(index_kolone_brojStranica);
            URL slika = null;
            try {
                slika = new URL(cursor.getString(index_kolone_slika));
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            boolean pregledan = false;
            if (cursor.getInt(index_kolone_pregledan) == 1)
                pregledan = true;
            ArrayList<Autor> autori = new ArrayList<>();
            autori.add(new Autor(imeAutora, Integer.toString(id)));

            if(idSvihKnjiga.contains(id)) {
                knjigice.add(new Knjiga(Integer.toString(id), naziv, autori, opis, datum, slika, brojStranica));
                knjigice.get(knjigice.size() - 1).setBoju(pregledan);
            }
        }
        cursor.close();
        db.close();

        return knjigice;
    }
}
