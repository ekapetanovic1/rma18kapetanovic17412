package ba.unsa.etf.rma.eldin.spirala17412;

import android.Manifest;
import android.app.Fragment;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class FragmentPreporuci extends Fragment {
    Spinner spinKontakti;
    Button dugmePosalji;
    TextView tekstNaziv;
    TextView tekstAutor;
    TextView tekstDatum;
    TextView tekstBrojStranica;
    TextView tekstOpis;
    ArrayList<Contact> kontakti = new ArrayList<>();
    ArrayList<String> emailKontakti = new ArrayList<>();
    ArrayAdapter<String> adapterKontakti;

    public FragmentPreporuci() {}

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View vi = inflater.inflate(R.layout.preporuci_fragment, container, false);

        requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, 1);

        return vi;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        spinKontakti = (Spinner)getView().findViewById(R.id.sKontakti);
        dugmePosalji = (Button)getView().findViewById(R.id.dPosalji);
        tekstNaziv = (TextView)getView().findViewById(R.id.txtNaziv);
        tekstAutor = (TextView)getView().findViewById(R.id.txtAutor);
        tekstDatum = (TextView)getView().findViewById(R.id.txtDatum);
        tekstBrojStranica = (TextView)getView().findViewById(R.id.txtBrojStranica);
        tekstOpis = (TextView)getView().findViewById(R.id.txtOpis);

        if(getArguments().containsKey("naziv") && getArguments().containsKey("autor") && getArguments().containsKey("datum") && getArguments().containsKey("opis") && getArguments().containsKey("brojStranica")) {
            String spoji = null;
            spoji = "Naziv knjige: " + getArguments().getString("naziv");
            tekstNaziv.setText(spoji);
            spoji = "Autor: " + getArguments().getString("autor");
            tekstAutor.setText(spoji);
            if(getArguments().getString("datum") != null) {
                spoji = "Datum objavljivanja: " + getArguments().getString("datum");
                tekstDatum.setText(spoji);
            }
            else
                tekstDatum.setText("");
            if(getArguments().getString("opis") != null) {
                spoji = "Opis: " + getArguments().getString("opis");
                tekstOpis.setText(spoji);
            }
            else
                tekstOpis.setText("");
            if(getArguments().getInt("brojStranica") != 0) {
                spoji = "Broj stranica: " + getArguments().getInt("brojStranica");
                tekstBrojStranica.setText(spoji);
            }
            else
                tekstBrojStranica.setText("");
        }

        try {
            ContentResolver cr = getContext().getContentResolver();
            Cursor cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                    String ime = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                    String email = null;
                    Cursor emailCursor = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?", new String[]{id}, null);
                    if (emailCursor != null && emailCursor.moveToFirst()) {
                        email = emailCursor.getString(emailCursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                        emailCursor.close();
                    }
                    kontakti.add(new Contact(id, ime, email));
                    emailKontakti.add(email);
                } while (cursor.moveToNext());

            }
                cursor.close();
        }
        catch (Exception ex) {
            Toast.makeText(getActivity(), "Greška kod email-a!", Toast.LENGTH_SHORT).show();
        }

        adapterKontakti = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, emailKontakti);
        spinKontakti.setAdapter(adapterKontakti);

        dugmePosalji.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ime = null;
                for(int i=0; i<kontakti.size(); i++)
                    if(kontakti.get(i).getEmail().equals(spinKontakti.getSelectedItem().toString())) {
                        ime = kontakti.get(i).getIme();
                        break;
                    }
                String[] TO = {spinKontakti.getSelectedItem().toString()};
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.setData(Uri.parse("mailto:"));
                emailIntent.setType("text/plain");
                emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
                emailIntent.putExtra(Intent.EXTRA_TEXT, "Zdravo " + ime + ",\nPročitaj knjigu " + getArguments().getString("naziv") + " od " + getArguments().getString("autor") + "!");
                try {
                    startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                }
                catch(android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getActivity(), "Nije instaliran email klijent!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch(requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    Log.e("Imate permisiju!","1");
                else
                    Log.e("Nemate permisiju!", "2");
            }
            return;
        }
    }
}
