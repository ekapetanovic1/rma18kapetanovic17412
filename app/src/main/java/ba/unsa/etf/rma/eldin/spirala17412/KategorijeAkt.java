package ba.unsa.etf.rma.eldin.spirala17412;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.facebook.stetho.Stetho;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import static ba.unsa.etf.rma.eldin.spirala17412.BazaOpenHelper.AUTOR_ID;
import static ba.unsa.etf.rma.eldin.spirala17412.BazaOpenHelper.AUTOR_IME;
import static ba.unsa.etf.rma.eldin.spirala17412.BazaOpenHelper.DATABASE_NAME;
import static ba.unsa.etf.rma.eldin.spirala17412.BazaOpenHelper.DATABASE_TABLE2;
import static ba.unsa.etf.rma.eldin.spirala17412.BazaOpenHelper.DATABASE_TABLE3;
import static ba.unsa.etf.rma.eldin.spirala17412.BazaOpenHelper.DATABASE_VERSION;
import static ba.unsa.etf.rma.eldin.spirala17412.BazaOpenHelper.KATEGORIJA_ID;
import static ba.unsa.etf.rma.eldin.spirala17412.BazaOpenHelper.KATEGORIJA_NAZIV;
import static ba.unsa.etf.rma.eldin.spirala17412.BazaOpenHelper.KNJIGA_NAZIV;

public class KategorijeAkt extends AppCompatActivity implements ListeFragment.iOnItemClick {
    public static BazaOpenHelper mojaBaza;
    public static ArrayList<String> listaKat = new ArrayList<>();
    public static ArrayList<Knjiga> listaKnjiga = new ArrayList<>();
    public static ArrayList<String> listaAutora = new ArrayList<>();
    public static Boolean kat;
    public static Boolean siriL;
    private ArrayList<Knjiga> knjigeAut = new ArrayList<>();
    private ArrayList<Knjiga> knjigeKat = new ArrayList<>();
    public static LinearLayout.LayoutParams parametar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Stetho.initializeWithDefaults(this);
        setContentView(R.layout.activity_kategorije_akt);

        //this.deleteDatabase(BazaOpenHelper.DATABASE_NAME);

        mojaBaza = new BazaOpenHelper(this, DATABASE_NAME, null, DATABASE_VERSION);

        kat=true;
        siriL=false;

        FragmentManager fm;
        fm = getFragmentManager();
        FrameLayout knjigaL = (FrameLayout)findViewById(R.id.knjigeF);

        if(knjigaL != null) {
            siriL = true;
            KnjigeFragment kf;
            kf = (KnjigeFragment) fm.findFragmentById(R.id.knjigeF);
            if (kf == null) {
                kf = new KnjigeFragment();
                fm.beginTransaction().replace(R.id.knjigeF, kf).commit();
            }
        }
        ListeFragment lf;
        lf = (ListeFragment) fm.findFragmentById(R.id.listeF);
        if (lf == null) {
            lf = new ListeFragment();
            fm.beginTransaction().replace(R.id.listeF, lf).commit();
        }
        else
            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    @Override
    public void onItemClicked(int pos) {
        knjigeKat.clear();
        knjigeAut.clear();
        int pozicija = -1;
        if(KategorijeAkt.kat) {
            String[] koloneRezultat = new String[]{KATEGORIJA_ID, KATEGORIJA_NAZIV};
            SQLiteDatabase db = mojaBaza.getReadableDatabase();
            Cursor cursor = db.query(BazaOpenHelper.DATABASE_TABLE1, koloneRezultat, null, null, null, null, null);
            int index_kolone_id = cursor.getColumnIndex(BazaOpenHelper.KATEGORIJA_ID);
            int index_kolone_naziv = cursor.getColumnIndex(BazaOpenHelper.KATEGORIJA_NAZIV);
            while (cursor.moveToNext()) {
                if(KategorijeAkt.listaKat.get(pos).equals(cursor.getString(index_kolone_naziv))) {
                    pozicija = cursor.getInt(index_kolone_id);
                    break;
                }
            }
            cursor.close();
            db.close();

            knjigeKat = mojaBaza.knjigeKategorije(pozicija);

            /*
            if (listaKat.size() != 0) {
                for (int i = 0; i < listaKnjiga.size(); i++)
                    if (listaKnjiga.get(i).getNazivKategorije().contentEquals(listaKat.get(pos)))
                        knjigeKat.add(listaKnjiga.get(i));
            }*/
        }
        else {
            String[] koloneRezultatAutor = new String[]{AUTOR_ID, AUTOR_IME};
            SQLiteDatabase db = mojaBaza.getReadableDatabase();
            Cursor cursor3 = db.query(DATABASE_TABLE3, koloneRezultatAutor, null, null, null, null, null);
            int index_kolone_idAutor = cursor3.getColumnIndex(AUTOR_ID);
            int index_kolone_imeAutor = cursor3.getColumnIndex(AUTOR_IME);
            while(cursor3.moveToNext()) {
                String imeAutora = cursor3.getString(index_kolone_imeAutor);
                if(KategorijeAkt.listaAutora.get(pos).equals(imeAutora)) {
                    pozicija = cursor3.getInt(index_kolone_idAutor);
                    break;
                }
            }
            cursor3.close();

            knjigeAut = mojaBaza.knjigeAutora(pozicija);

            db.close();
            /*
            if (listaAutora.size() != 0) {
                for (int i = 0; i < listaKnjiga.size(); i++)
                    if (listaKnjiga.get(i).getImeAutora().contentEquals(listaAutora.get(pos)))
                        knjigeAut.add(listaKnjiga.get(i));
            }*/
        }
        Bundle argumenti = new Bundle();
        argumenti.putParcelableArrayList("knjigeKategorije", knjigeKat);
        argumenti.putParcelableArrayList("knjigeAutora", knjigeAut);
        KnjigeFragment kf2 = new KnjigeFragment();
        kf2.setArguments(argumenti);
        if(siriL)
            getFragmentManager().beginTransaction().replace(R.id.knjigeF, kf2).commit();
        else
            getFragmentManager().beginTransaction().replace(R.id.listeF, kf2).addToBackStack(null).commit();
    }

    /*      dodajKategoriju = (Button) findViewById(R.id.dDodajKategoriju);
        dodajKategoriju.setEnabled(false);

        Intent i = this.getIntent();
        if(!(i.getStringArrayListExtra("sveKategorije") == null))
            listaKat = i.getStringArrayListExtra("sveKategorije");
        else if(!(i.getStringArrayListExtra("nekeKategorije") == null))
            listaKat = i.getStringArrayListExtra("nekeKategorije");

        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listaKat);
        listaKatView = (ListView)findViewById(R.id.listaKategorija);
        listaKatView.setAdapter(adapter);

        pretraga = (Button)findViewById(R.id.dPretraga);
        dodajKnjigu = (Button)findViewById(R.id.dDodajKnjigu);
        txtPretraga = (EditText)findViewById(R.id.tekstPretraga);

        pretraga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!txtPretraga.equals("")) {
                    adapter.getFilter().filter(txtPretraga.getText().toString(), new Filter.FilterListener() {
                        @Override
                        public void onFilterComplete(int count) {
                            if(count == 0) dodajKategoriju.setEnabled(true);
                        }
                    });
                }
            }
        });

        dodajKategoriju.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listaKat.add(txtPretraga.getText().toString());
                adapter.clear();
                adapter.addAll(listaKat);
                adapter.notifyDataSetChanged();
                txtPretraga.setText("");
                pretraga.performClick();
                dodajKategoriju.setEnabled(false);
            }
        });

        dodajKnjigu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent knjigaIntent = new Intent(KategorijeAkt.this, DodavanjeKnjigeAkt.class);
                knjigaIntent.putStringArrayListExtra("sveKategorije", listaKat);
                KategorijeAkt.this.startActivity(knjigaIntent);
            }
        });

        listaKatView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent listaKnjigaIntent = new Intent(KategorijeAkt.this, ListaKnjigaAkt.class);
                ArrayList<Knjiga> listica = new ArrayList<>();
                for(int j=0; j < listaKnjiga.size(); j++)
                    if(listaKnjiga.get(j).getNazivKategorije().equals(adapter.getItem(i).toString()))
                        listica.add(listaKnjiga.get(j));
                listaKnjigaIntent.putExtra("izabraneKnjige", listica);
                listaKnjigaIntent.putStringArrayListExtra("izabraneKategorije", listaKat);
                KategorijeAkt.this.startActivity(listaKnjigaIntent);
            }
        });*/
}
