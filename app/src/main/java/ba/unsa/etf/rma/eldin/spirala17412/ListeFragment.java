package ba.unsa.etf.rma.eldin.spirala17412;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static ba.unsa.etf.rma.eldin.spirala17412.BazaOpenHelper.AUTORSTVO_ID;
import static ba.unsa.etf.rma.eldin.spirala17412.BazaOpenHelper.AUTORSTVO_IDAUTORA;
import static ba.unsa.etf.rma.eldin.spirala17412.BazaOpenHelper.AUTORSTVO_IDKNJIGE;
import static ba.unsa.etf.rma.eldin.spirala17412.BazaOpenHelper.AUTOR_ID;
import static ba.unsa.etf.rma.eldin.spirala17412.BazaOpenHelper.AUTOR_IME;
import static ba.unsa.etf.rma.eldin.spirala17412.BazaOpenHelper.DATABASE_NAME;
import static ba.unsa.etf.rma.eldin.spirala17412.BazaOpenHelper.DATABASE_TABLE2;
import static ba.unsa.etf.rma.eldin.spirala17412.BazaOpenHelper.DATABASE_TABLE3;
import static ba.unsa.etf.rma.eldin.spirala17412.BazaOpenHelper.DATABASE_TABLE4;
import static ba.unsa.etf.rma.eldin.spirala17412.BazaOpenHelper.DATABASE_VERSION;
import static ba.unsa.etf.rma.eldin.spirala17412.BazaOpenHelper.KATEGORIJA_ID;
import static ba.unsa.etf.rma.eldin.spirala17412.BazaOpenHelper.KATEGORIJA_NAZIV;
import static ba.unsa.etf.rma.eldin.spirala17412.BazaOpenHelper.KNJIGA_BROJSTRANICA;
import static ba.unsa.etf.rma.eldin.spirala17412.BazaOpenHelper.KNJIGA_DATUMOBJAVLJIVANJA;
import static ba.unsa.etf.rma.eldin.spirala17412.BazaOpenHelper.KNJIGA_ID;
import static ba.unsa.etf.rma.eldin.spirala17412.BazaOpenHelper.KNJIGA_IDKATEGORIJE;
import static ba.unsa.etf.rma.eldin.spirala17412.BazaOpenHelper.KNJIGA_IDWEBSERVIS;
import static ba.unsa.etf.rma.eldin.spirala17412.BazaOpenHelper.KNJIGA_NAZIV;
import static ba.unsa.etf.rma.eldin.spirala17412.BazaOpenHelper.KNJIGA_OPIS;
import static ba.unsa.etf.rma.eldin.spirala17412.BazaOpenHelper.KNJIGA_PREGLEDANA;
import static ba.unsa.etf.rma.eldin.spirala17412.BazaOpenHelper.KNJIGA_SLIKA;
import static ba.unsa.etf.rma.eldin.spirala17412.KategorijeAkt.listaKnjiga;
import static ba.unsa.etf.rma.eldin.spirala17412.KategorijeAkt.mojaBaza;

public class ListeFragment extends Fragment {
    Button prikazKategorija;
    Button prikazAutora;
    Button pretraga;
    Button dodajKategoriju;
    Button dodajKnjigu;
    ListView listaKatView;
    EditText txtPretraga;
    ArrayAdapter<String> adapter;
    ArrayList<String> kategorijeLista = new ArrayList<>();
    private iOnItemClick iOic;
    Button dodajKnjiguOnline;

    public ListeFragment() {}

    public interface iOnItemClick {
        public void onItemClicked(int pos);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View vi = inflater.inflate(R.layout.liste_fragment, container, false);

        dodajKategoriju = (Button)vi.findViewById(R.id.dDodajKategoriju);
        dodajKategoriju.setEnabled(false);

        dodajKnjigu = (Button)vi.findViewById(R.id.dDodajKnjigu);
        dodajKnjigu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm1 = getFragmentManager();

                if(KategorijeAkt.siriL) {
                    FrameLayout fl = (FrameLayout)getActivity().findViewById(R.id.listeF);
                    KategorijeAkt.parametar = (LinearLayout.LayoutParams)fl.getLayoutParams();
                    LinearLayout.LayoutParams flparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                    fl.setLayoutParams(flparams);
                }
                DodavanjeKnjigeFragment dkf = new DodavanjeKnjigeFragment();
                fm1.beginTransaction().replace(R.id.listeF, dkf).addToBackStack(null).commit();
            }
        });

        dodajKnjiguOnline = (Button)vi.findViewById(R.id.dDodajOnline);
        dodajKnjiguOnline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm1 = getFragmentManager();

                if(KategorijeAkt.siriL) {
                    FrameLayout fl = (FrameLayout)getActivity().findViewById(R.id.listeF);
                    KategorijeAkt.parametar = (LinearLayout.LayoutParams)fl.getLayoutParams();
                    LinearLayout.LayoutParams flparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                    fl.setLayoutParams(flparams);
                }
                FragmentOnline fo = new FragmentOnline();
                fm1.beginTransaction().replace(R.id.listeF, fo).addToBackStack(null).commit();
            }
        });

        return vi;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        prikazKategorija = (Button)getView().findViewById(R.id.dKategorije);
        prikazAutora = (Button)getView().findViewById(R.id.dAutori);
        pretraga = (Button)getView().findViewById(R.id.dPretraga);
        dodajKategoriju = (Button)getView().findViewById(R.id.dDodajKategoriju);
        txtPretraga = (EditText)getView().findViewById(R.id.tekstPretraga);

        KategorijeAkt.kat = true;
        KategorijeAkt.listaKat.clear();
        KategorijeAkt.listaAutora.clear();

        String[] koloneRezultat = new String[]{KATEGORIJA_ID, KATEGORIJA_NAZIV};
        SQLiteDatabase db = mojaBaza.getReadableDatabase();
        Cursor cursor = db.query(BazaOpenHelper.DATABASE_TABLE1, koloneRezultat, null, null, null, null, null);
        int index_kolone_naziv = cursor.getColumnIndex(BazaOpenHelper.KATEGORIJA_NAZIV);
        while (cursor.moveToNext())
            KategorijeAkt.listaKat.add(cursor.getString(index_kolone_naziv));
        cursor.close();

        String[] koloneRezultatAutor = new String[]{AUTOR_ID, AUTOR_IME};
        Cursor cursor2 = db.query(DATABASE_TABLE3, koloneRezultatAutor, null, null, null, null, null);
        int index_kolone_ime = cursor2.getColumnIndex(AUTOR_IME);
        while(cursor2.moveToNext()){
            KategorijeAkt.listaAutora.add(cursor2.getString(index_kolone_ime));
        }
        cursor2.close();

        listaKnjiga.clear();

        db = mojaBaza.getReadableDatabase();
        String[] koloneRezultatKnjiga = new String[]{KNJIGA_ID, KNJIGA_NAZIV, KNJIGA_OPIS, KNJIGA_DATUMOBJAVLJIVANJA, KNJIGA_BROJSTRANICA, KNJIGA_IDWEBSERVIS, KNJIGA_IDKATEGORIJE, KNJIGA_SLIKA, KNJIGA_PREGLEDANA};
        Cursor cursor3 = db.query(DATABASE_TABLE2, koloneRezultatKnjiga, null, null, null, null, null);
        int index_kolone_id = cursor3.getColumnIndex(KNJIGA_ID);
        int index_kolone_naziv_knjige = cursor3.getColumnIndex(KNJIGA_NAZIV);
        int index_kolone_opis = cursor3.getColumnIndex(KNJIGA_OPIS);
        int index_kolone_datum = cursor3.getColumnIndex(KNJIGA_DATUMOBJAVLJIVANJA);
        int index_kolone_brojStranica = cursor3.getColumnIndex(KNJIGA_BROJSTRANICA);
        int index_kolone_slika = cursor3.getColumnIndex(KNJIGA_SLIKA);
        int index_kolone_pregledan = cursor3.getColumnIndex(KNJIGA_PREGLEDANA);
        while (cursor3.moveToNext()) {
            String id = cursor3.getString(index_kolone_id);
            String naziv = cursor3.getString(index_kolone_naziv_knjige);
            String opis = cursor3.getString(index_kolone_opis);
            String datum = cursor3.getString(index_kolone_datum);
            int brojStranica = cursor3.getInt(index_kolone_brojStranica);
            URL slika = null;
            try {
                slika = new URL(cursor3.getString(index_kolone_slika));
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            boolean pregledan = false;
            if (cursor3.getInt(index_kolone_pregledan) == 1)
                pregledan = true;

            ArrayList<Autor> autori = new ArrayList<>();
            List<Integer> idSvihAutora = new ArrayList<Integer>();
            String[] koloneRezultatAutorstvo = new String[]{AUTORSTVO_ID, AUTORSTVO_IDAUTORA, AUTORSTVO_IDKNJIGE};
            String where2 = AUTORSTVO_IDKNJIGE + "=" + id;
            Cursor cursor4 = db.query(DATABASE_TABLE4, koloneRezultatAutorstvo, where2, null, null, null, null);
            int index_kolone_idAutora = cursor4.getColumnIndex(AUTORSTVO_IDAUTORA);
            while(cursor4.moveToNext())
                idSvihAutora.add(cursor4.getInt(index_kolone_idAutora));
            cursor4.close();

            String[] koloneRezultatAutor2 = new String[]{AUTOR_ID, AUTOR_IME};
            Cursor cursor5 = db.query(DATABASE_TABLE3, koloneRezultatAutor2, null, null, null, null, null);
            int index_kolone_idAutor = cursor5.getColumnIndex(AUTOR_ID);
            int index_kolone_imeAutor = cursor5.getColumnIndex(AUTOR_IME);
            while(cursor5.moveToNext()) {
                int noviIdAutora = cursor5.getInt(index_kolone_idAutor);
                if(idSvihAutora.contains(noviIdAutora))
                    autori.add(new Autor(cursor5.getString(index_kolone_imeAutor), id));
            }
            cursor5.close();

            listaKnjiga.add(new Knjiga(id, naziv, autori, opis, datum, slika, brojStranica));
            listaKnjiga.get(listaKnjiga.size()-1).setBoju(pregledan);
        }
        cursor3.close();
        db.close();

        adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, KategorijeAkt.listaKat);
        listaKatView = (ListView)getView().findViewById(R.id.listaKategorija);
        listaKatView.setAdapter(adapter);

        try {
            iOic = (iOnItemClick)getActivity();
        }
        catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString() + "Treba implementirati iOnItemClick");
        }

        listaKatView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                iOic.onItemClicked(position);
            }
        });

        prikazAutora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pretraga.setVisibility(view.GONE);
                dodajKategoriju.setVisibility(view.GONE);
                txtPretraga.setVisibility(view.GONE);

                KategorijeAkt.kat = false;
                kategorijeLista.clear();

                listaKnjiga.clear();

                SQLiteDatabase db = mojaBaza.getReadableDatabase();
                String[] koloneRezultatKnjiga = new String[]{KNJIGA_ID, KNJIGA_NAZIV, KNJIGA_OPIS, KNJIGA_DATUMOBJAVLJIVANJA, KNJIGA_BROJSTRANICA, KNJIGA_IDWEBSERVIS, KNJIGA_IDKATEGORIJE, KNJIGA_SLIKA, KNJIGA_PREGLEDANA};
                Cursor cursor = db.query(DATABASE_TABLE2, koloneRezultatKnjiga, null, null, null, null, null);
                int index_kolone_id = cursor.getColumnIndex(KNJIGA_ID);
                int index_kolone_naziv = cursor.getColumnIndex(KNJIGA_NAZIV);
                int index_kolone_opis = cursor.getColumnIndex(KNJIGA_OPIS);
                int index_kolone_datum = cursor.getColumnIndex(KNJIGA_DATUMOBJAVLJIVANJA);
                int index_kolone_brojStranica = cursor.getColumnIndex(KNJIGA_BROJSTRANICA);
                int index_kolone_slika = cursor.getColumnIndex(KNJIGA_SLIKA);
                int index_kolone_pregledan = cursor.getColumnIndex(KNJIGA_PREGLEDANA);
                while (cursor.moveToNext()) {
                    String id = cursor.getString(index_kolone_id);
                    String naziv = cursor.getString(index_kolone_naziv);
                    String opis = cursor.getString(index_kolone_opis);
                    String datum = cursor.getString(index_kolone_datum);
                    int brojStranica = cursor.getInt(index_kolone_brojStranica);
                    URL slika = null;
                    try {
                        slika = new URL(cursor.getString(index_kolone_slika));
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                    boolean pregledan = false;
                    if (cursor.getInt(index_kolone_pregledan) == 1)
                        pregledan = true;

                    ArrayList<Autor> autori = new ArrayList<>();
                    List<Integer> idSvihAutora = new ArrayList<Integer>();
                    String[] koloneRezultatAutorstvo = new String[]{AUTORSTVO_ID, AUTORSTVO_IDAUTORA, AUTORSTVO_IDKNJIGE};
                    String where2 = AUTORSTVO_IDKNJIGE + "=" + id;
                    Cursor cursor2 = db.query(DATABASE_TABLE4, koloneRezultatAutorstvo, where2, null, null, null, null);
                    int index_kolone_idAutora = cursor2.getColumnIndex(AUTORSTVO_IDAUTORA);
                    while(cursor2.moveToNext())
                        idSvihAutora.add(cursor2.getInt(index_kolone_idAutora));
                    cursor2.close();

                    String[] koloneRezultatAutor = new String[]{AUTOR_ID, AUTOR_IME};
                    Cursor cursor3 = db.query(DATABASE_TABLE3, koloneRezultatAutor, null, null, null, null, null);
                    int index_kolone_idAutor = cursor3.getColumnIndex(AUTOR_ID);
                    int index_kolone_imeAutor = cursor3.getColumnIndex(AUTOR_IME);
                    while(cursor3.moveToNext()) {
                        int noviIdAutora = cursor3.getInt(index_kolone_idAutor);
                        if(idSvihAutora.contains(noviIdAutora))
                            autori.add(new Autor(cursor3.getString(index_kolone_imeAutor), id));
                    }
                    cursor3.close();

                    listaKnjiga.add(new Knjiga(id, naziv, autori, opis, datum, slika, brojStranica));
                    listaKnjiga.get(listaKnjiga.size()-1).setBoju(pregledan);
                }
                cursor.close();
                db.close();

                for(int k=0; k < KategorijeAkt.listaKnjiga.size(); k++) {
                    if (KategorijeAkt.listaAutora.size() != 0) {
                        if(KategorijeAkt.listaKnjiga.get(k).getAutori() != null) {
                            for(int h=0; h<KategorijeAkt.listaKnjiga.get(k).getAutori().size(); h++) {
                                if(!KategorijeAkt.listaAutora.contains(KategorijeAkt.listaKnjiga.get(k).getAutori().get(h).getImeiPrezime()))
                                    KategorijeAkt.listaAutora.add(KategorijeAkt.listaKnjiga.get(k).getAutori().get(h).getImeiPrezime());
                            }
                        }
                        else if (!KategorijeAkt.listaAutora.contains(KategorijeAkt.listaKnjiga.get(k).getImeAutora()))
                            KategorijeAkt.listaAutora.add(KategorijeAkt.listaKnjiga.get(k).getImeAutora());
                    }
                    else
                        KategorijeAkt.listaAutora.add(KategorijeAkt.listaKnjiga.get(k).getImeAutora());
                }
                for(int i=0; i < KategorijeAkt.listaAutora.size(); i++) {
                    int broj = 0;
                    for(int j=0; j < KategorijeAkt.listaKnjiga.size(); j++) {
                        if(KategorijeAkt.listaKnjiga.get(j).getAutori() != null) {
                            for(int k=0; k<KategorijeAkt.listaKnjiga.get(j).getAutori().size(); k++)
                                if(KategorijeAkt.listaKnjiga.get(j).getAutori().get(k).getImeiPrezime().equals(KategorijeAkt.listaAutora.get(i)))
                                    broj++;
                        }
                        else if(KategorijeAkt.listaKnjiga.get(j).getImeAutora().equals(KategorijeAkt.listaAutora.get(i)))
                            broj++;
                    }
                    kategorijeLista.add(KategorijeAkt.listaAutora.get(i) + " - " + broj);
                }

                adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, kategorijeLista);
                listaKatView = (ListView)getView().findViewById(R.id.listaKategorija);
                listaKatView.setAdapter(adapter);
            }
        });

        prikazKategorija.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pretraga.setVisibility(view.VISIBLE);
                dodajKategoriju.setVisibility(view.VISIBLE);
                txtPretraga.setVisibility(view.VISIBLE);
                dodajKategoriju.setEnabled(false);

                KategorijeAkt.kat = true;

                adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, KategorijeAkt.listaKat);
                listaKatView = (ListView)getView().findViewById(R.id.listaKategorija);
                listaKatView.setAdapter(adapter);
            }
        });

        pretraga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!txtPretraga.equals("")) {
                    adapter.getFilter().filter(txtPretraga.getText().toString(), new Filter.FilterListener() {
                        @Override
                        public void onFilterComplete(int count) {
                            if(count == 0) dodajKategoriju.setEnabled(true);
                        }
                    });
                }
            }
        });

        dodajKategoriju.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if(mojaBaza.dodajKategoriju(txtPretraga.getText().toString()) == -1)
                        throw new Exception("Pogreška pri dodavanju kategorije u bazu podataka!");
                }
                catch (Exception e) {
                    Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
                }
                KategorijeAkt.listaKat.add(txtPretraga.getText().toString());
                adapter.clear();
                adapter.addAll(KategorijeAkt.listaKat);
                adapter.notifyDataSetChanged();
                txtPretraga.setText("");
                pretraga.performClick();
                dodajKategoriju.setEnabled(false);
            }
        });
/*
        listaKatView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent listaKnjigaIntent = new Intent(KategorijeAkt.this, ListaKnjigaAkt.class);
                ArrayList<Knjiga> listica = new ArrayList<>();
                for(int j=0; j < listaKnjiga.size(); j++)
                    if(listaKnjiga.get(j).getNazivKategorije().equals(adapter.getItem(i).toString()))
                        listica.add(listaKnjiga.get(j));
                listaKnjigaIntent.putExtra("izabraneKnjige", listica);
                listaKnjigaIntent.putStringArrayListExtra("izabraneKategorije", listaKat);
                KategorijeAkt.this.startActivity(listaKnjigaIntent);
            }
        });*/
    }
}
