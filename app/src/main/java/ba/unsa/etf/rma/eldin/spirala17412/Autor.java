package ba.unsa.etf.rma.eldin.spirala17412;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class Autor implements Parcelable {
    private String imeiPrezime;
    private ArrayList<String> knjige = new ArrayList<>();

    public Autor(String _imeiPrezime, String id) {
        imeiPrezime = _imeiPrezime;
        knjige.add(id);
    }

    public void dodajKnjigu(String id) { if(!knjige.contains(id)) knjige.add(id); }

    protected Autor(Parcel in) {
        Bundle mojBundle = in.readBundle();

        imeiPrezime = mojBundle.getString("imeiPrezime");
        knjige = mojBundle.getStringArrayList("knjige");
    }

    public static final Creator<Autor> CREATOR = new Creator<Autor>() {
        @Override
        public Autor createFromParcel(Parcel in) {
            return new Autor(in);
        }

        @Override
        public Autor[] newArray(int i) {
            return new Autor[i];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int i) {
        Bundle mojBundle = new Bundle();
        mojBundle.putString("imeiPrezime", imeiPrezime);
        mojBundle.putStringArrayList("knjige", knjige);

        dest.writeBundle(mojBundle);
    }


    public String getImeiPrezime() { return imeiPrezime; }

    public ArrayList<String> getKnjige() { return knjige; }

    public void setImeiPrezime(String _ime) { imeiPrezime = _ime; }

    public void setKnjige(ArrayList<String> _knjige) { knjige = _knjige; }
}
