package ba.unsa.etf.rma.eldin.spirala17412;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

import java.net.URL;
import java.util.ArrayList;

public class Knjiga implements Parcelable {
    private String id;
    private String naziv;
    private ArrayList<Autor> autori = new ArrayList<>();
    private String opis;
    private String datumObjavljivanja;
    private URL slika;
    private int brojStranica;

    private String imeAutora;
    private String nazivKategorije;
    private Boolean obojen;

    public Knjiga(String _id, String _naziv, ArrayList<Autor> _autori, String _opis, String _datumObjavljivanja, URL _slika, int _brojStranica) {
        id =_id;
        naziv = _naziv;
        autori = _autori;
        opis = _opis;
        datumObjavljivanja = _datumObjavljivanja;
        slika = _slika;
        brojStranica = _brojStranica;
        if(_autori != null)
            imeAutora = _autori.get(0).getImeiPrezime();
        else
            imeAutora = null;
        nazivKategorije = null;
        obojen = false;
    }

    public Knjiga(String _ime, String _naziv, String _kategorija, Boolean _boja) {
        imeAutora = _ime;
        naziv = _naziv;
        nazivKategorije = _kategorija;
        obojen = _boja;

        id = null;
        autori = null;
        opis = null;
        datumObjavljivanja = null;
        slika = null;
        brojStranica = 0;
    }

    protected Knjiga(Parcel in) {
        obojen = in.readByte() != 0;
        Bundle mojBundle = in.readBundle();

        id = mojBundle.getString("id");
        naziv = mojBundle.getString("naziv");
        autori = mojBundle.getParcelableArrayList("autori");
        opis = mojBundle.getString("opis");
        datumObjavljivanja = mojBundle.getString("datumObjavljivanja");
        slika = null;
        brojStranica = mojBundle.getInt("brojStranica");
        imeAutora = mojBundle.getString("imeAutora");
        nazivKategorije = mojBundle.getString("nazivKategorije");
    }

    public static final Creator<Knjiga> CREATOR = new Creator<Knjiga>() {
        @Override
        public Knjiga createFromParcel(Parcel in) {
            return new Knjiga(in);
        }

        @Override
        public Knjiga[] newArray(int i) {
            return new Knjiga[i];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int i) {
        Bundle mojBundle = new Bundle();
        mojBundle.putString("id", id);
        mojBundle.putString("naziv", naziv);
        mojBundle.putParcelableArrayList("autori", autori);
        mojBundle.putString("opis", opis);
        mojBundle.putString("datumObjavljivanja", datumObjavljivanja);
        mojBundle.putInt("brojStranica", brojStranica);
        mojBundle.putString("imeAutora", imeAutora);
        mojBundle.putString("nazivKategorije", nazivKategorije);

        dest.writeByte((byte) (obojen ? 1 : 0));
        dest.writeBundle(mojBundle);
    }

    public String getId() { return id; }
    public String getNaziv() { return naziv; }
    public ArrayList<Autor> getAutori() { return autori; }
    public String getOpis() { return opis; }
    public String getDatumObjavljivanja() { return datumObjavljivanja; }
    public URL getSlika() { return slika; }
    public int getBrojStranica() { return brojStranica; }

    public void setId(String id) { this.id = id; }
    public void setNaziv(String naziv) { this.naziv = naziv; }
    public void setAutori(ArrayList<Autor> autori) { this.autori = autori; }
    public void setOpis(String opis) { this.opis = opis; }
    public void setDatumObjavljivanja(String datumObjavljivanja) { this.datumObjavljivanja = datumObjavljivanja; }
    public void setSlika(URL slika) { this.slika = slika; }
    public void setBrojStranica(int brojStranica) { this.brojStranica = brojStranica; }

    public String getImeAutora() { return imeAutora; }
    public String getNazivKategorije() { return nazivKategorije; }
    public Boolean getBoju() { return obojen; }

    public void setImeAutora(String ime) { imeAutora= ime; }
    public void setNazivKategorije(String nazivKat) { nazivKategorije=nazivKat; }
    public void setBoju(Boolean boja) { obojen = boja; }
}
