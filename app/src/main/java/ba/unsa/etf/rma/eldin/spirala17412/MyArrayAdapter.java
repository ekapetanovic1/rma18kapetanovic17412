package ba.unsa.etf.rma.eldin.spirala17412;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.FileNotFoundException;
import java.util.ArrayList;

public class MyArrayAdapter extends BaseAdapter {
    ArrayList<Knjiga> knjige;
    Activity context;

    public MyArrayAdapter(Activity context, ArrayList<Knjiga> knjige) {
        this.context = context;
        this.knjige = knjige;
    }

    @Override
    public int getCount() { return knjige.size(); }

    @Override
    public Knjiga getItem(int position) { return knjige.get(position); }

    @Override
    public long getItemId(int position) { return position; }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        RelativeLayout newView;

        if(convertView == null) {
            newView = new RelativeLayout(context);
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater li;
            li = (LayoutInflater)context.getSystemService(inflater);
            li.inflate(R.layout.element_liste_kategorije, newView, true);
        }
        else
            newView = (RelativeLayout)convertView;

        final Knjiga instanca = knjige.get(position);

        ImageView slika = (ImageView)newView.findViewById(R.id.eNaslovna);
        TextView naziv = (TextView)newView.findViewById(R.id.eNaziv);
        TextView autor = (TextView)newView.findViewById(R.id.eAutor);
        TextView datum = (TextView)newView.findViewById(R.id.eDatumObjavljivanja);
        TextView opis = (TextView)newView.findViewById(R.id.eOpis);
        TextView brojStranica = (TextView)newView.findViewById(R.id.eBrojStranica);
        Button dugmePreporuka = (Button)newView.findViewById(R.id.dPreporuci);

        dugmePreporuka.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = context.getFragmentManager();

                if(KategorijeAkt.siriL) {
                    FrameLayout fl = (FrameLayout)v.findViewById(R.id.listeF);
                    KategorijeAkt.parametar = (LinearLayout.LayoutParams)fl.getLayoutParams();
                    LinearLayout.LayoutParams flparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                    fl.setLayoutParams(flparams);
                }
                FragmentPreporuci fp = new FragmentPreporuci();
                Bundle mojBundle = new Bundle();
                mojBundle.putString("naziv", instanca.getNaziv());
                mojBundle.putString("autor", instanca.getImeAutora());
                mojBundle.putString("datum", instanca.getDatumObjavljivanja());
                mojBundle.putString("opis", instanca.getOpis());
                mojBundle.putInt("brojStranica", instanca.getBrojStranica());
                fp.setArguments(mojBundle);
                fm.beginTransaction().replace(R.id.listeF, fp).addToBackStack(null).commit();
            }
        });

        naziv.setText(instanca.getNaziv());
        autor.setText(instanca.getImeAutora());
        if(instanca.getDatumObjavljivanja() != null)
            datum.setText(instanca.getDatumObjavljivanja());
        else
            datum.setText("");
        if(instanca.getOpis() != null)
            opis.setText(instanca.getOpis());
        else
            opis.setText("");
        if(instanca.getBrojStranica() != 0)
            brojStranica.setText(String.valueOf(instanca.getBrojStranica()));
        else
            brojStranica.setText("");
        if(instanca.getDatumObjavljivanja() == null && instanca.getOpis() == null && instanca.getBrojStranica() == 0) {
            try {
                slika.setImageBitmap(BitmapFactory.decodeStream(parent.getContext().openFileInput(instanca.getNaziv())));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        else if(instanca.getSlika() != null)
            Picasso.get().load(instanca.getSlika().toString()).into(slika);

        Boolean boja = instanca.getBoju();
        if(boja) {
            newView.setBackgroundResource(R.color.bojaListePozadina);
            this.notifyDataSetChanged();
        }

        return newView;
    }
}
