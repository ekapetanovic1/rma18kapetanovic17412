package ba.unsa.etf.rma.eldin.spirala17412;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

public class DodavanjeKnjigeAkt extends AppCompatActivity {
    ArrayList<String> listaKategorija = new ArrayList<>();
    ArrayAdapter<String> adapterKategorije;
    EditText ime;
    EditText naziv;
    Button nadjiSliku;
    Button upisiKnjigu;
    Button ponistiAkt;
    Spinner katSpiner;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dodavanje_knjige_akt);

        ime = (EditText) findViewById(R.id.imeAutora);
        naziv = (EditText) findViewById(R.id.nazivKnjige);
        nadjiSliku = (Button) findViewById(R.id.dNadjiSliku);
        upisiKnjigu = (Button) findViewById(R.id.dUpisiKnjigu);
        ponistiAkt = (Button) findViewById(R.id.dPonisti);
        katSpiner = (Spinner) findViewById(R.id.sKategorijaKnjige);
        imageView = (ImageView) findViewById(R.id.naslovnaStr);
        imageView.setImageResource(R.mipmap.ic_launcher);

        Intent i = this.getIntent();
        listaKategorija = i.getStringArrayListExtra("sveKategorije");

        adapterKategorije = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, listaKategorija);
        katSpiner.setAdapter(adapterKategorije);

        upisiKnjigu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ime.getText().toString().equals(""))
                    Toast.makeText(DodavanjeKnjigeAkt.this, "Polje za unos imena autora je prazno!", Toast.LENGTH_SHORT).show();
                else if (naziv.getText().toString().equals(""))
                    Toast.makeText(DodavanjeKnjigeAkt.this, "Polje za unos naziva knjige je prazno!", Toast.LENGTH_SHORT).show();
                else if (listaKategorija.size() == 0 || listaKategorija == null)
                    Toast.makeText(DodavanjeKnjigeAkt.this, "Polje za izbor kategorije je prazno!", Toast.LENGTH_SHORT).show();
                else {
                    KategorijeAkt.listaKnjiga.add(new Knjiga(ime.getText().toString(), naziv.getText().toString(), katSpiner.getSelectedItem().toString(), false));
                    ime.setText("");
                    naziv.setText("");
                    katSpiner.setSelection(0);
                    ime.requestFocus();
                    imageView.setImageResource(R.mipmap.ic_launcher);
                    Toast.makeText(DodavanjeKnjigeAkt.this, "Knjiga uspješno registrovana!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        ponistiAkt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ponistiIntent = new Intent(DodavanjeKnjigeAkt.this, KategorijeAkt.class);
                ponistiIntent.putStringArrayListExtra("sveKategorije", listaKategorija);
                DodavanjeKnjigeAkt.this.startActivity(ponistiIntent);
            }
        });

        nadjiSliku.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent slikaIntent = new Intent();
                slikaIntent.setType("image/*");
                slikaIntent.setAction(Intent.ACTION_GET_CONTENT);
                //slikaIntent.addCategory(Intent.CATEGORY_OPENABLE);
                startActivityForResult(slikaIntent, 1);
            }
        });
    }

    Bitmap getBitmapFromUri (Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor = getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return image;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EditText naziv2 = (EditText)findViewById(R.id.nazivKnjige);
        imageView = (ImageView) findViewById(R.id.naslovnaStr);

        if (requestCode == 1 && resultCode == RESULT_OK && data != null && data.getData() != null) {
            try {
                FileOutputStream outputStream;
                outputStream = openFileOutput(naziv2.getText().toString(), Context.MODE_PRIVATE);
                getBitmapFromUri(data.getData()).compress(Bitmap.CompressFormat.JPEG,90,outputStream);
                outputStream.close();

                imageView.setImageBitmap(BitmapFactory.decodeStream(openFileInput(naziv2.getText().toString())));
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
