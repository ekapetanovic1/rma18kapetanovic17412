package ba.unsa.etf.rma.eldin.spirala17412;

public class Contact {
    private String id;
    private String ime;
    private String email;

    public Contact(String _id, String _ime, String _email) {
        id = _id;
        ime = _ime;
        email = _email;
    }

    public String getId() { return id; }
    public String getIme() { return ime; }
    public String getEmail() { return email; }
    public void setId(String _id) { id = _id; }
    public void setIme(String _ime) { ime = _ime; }
    public void setEmail(String _email) { email = _email; }
}
