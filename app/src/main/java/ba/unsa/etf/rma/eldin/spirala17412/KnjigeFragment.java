package ba.unsa.etf.rma.eldin.spirala17412;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Toast;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import static ba.unsa.etf.rma.eldin.spirala17412.BazaOpenHelper.DATABASE_TABLE2;
import static ba.unsa.etf.rma.eldin.spirala17412.BazaOpenHelper.KNJIGA_BROJSTRANICA;
import static ba.unsa.etf.rma.eldin.spirala17412.BazaOpenHelper.KNJIGA_DATUMOBJAVLJIVANJA;
import static ba.unsa.etf.rma.eldin.spirala17412.BazaOpenHelper.KNJIGA_ID;
import static ba.unsa.etf.rma.eldin.spirala17412.BazaOpenHelper.KNJIGA_IDKATEGORIJE;
import static ba.unsa.etf.rma.eldin.spirala17412.BazaOpenHelper.KNJIGA_IDWEBSERVIS;
import static ba.unsa.etf.rma.eldin.spirala17412.BazaOpenHelper.KNJIGA_NAZIV;
import static ba.unsa.etf.rma.eldin.spirala17412.BazaOpenHelper.KNJIGA_OPIS;
import static ba.unsa.etf.rma.eldin.spirala17412.BazaOpenHelper.KNJIGA_PREGLEDANA;
import static ba.unsa.etf.rma.eldin.spirala17412.BazaOpenHelper.KNJIGA_SLIKA;
import static ba.unsa.etf.rma.eldin.spirala17412.KategorijeAkt.mojaBaza;

public class KnjigeFragment extends Fragment {
    ListView listaOdabranihKnjiga;
    Button povratakDugme;
    MyArrayAdapter mojAdapter;
    private ArrayList<Knjiga> knjigiceKat = new ArrayList<>();
    private ArrayList<Knjiga> knjigiceAut = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View vi2 = inflater.inflate(R.layout.activity_lista_knjiga_akt, container, false);

        listaOdabranihKnjiga = (ListView)vi2.findViewById(R.id.listaKnjiga);

        if(getArguments() != null && (getArguments().containsKey("knjigeKategorije") || getArguments().containsKey("knjigeAutora"))) {
            knjigiceKat = getArguments().getParcelableArrayList("knjigeKategorije");
            knjigiceAut = getArguments().getParcelableArrayList("knjigeAutora");

            if(KategorijeAkt.kat)
                mojAdapter = new MyArrayAdapter(getActivity(), knjigiceKat);
            else
                mojAdapter = new MyArrayAdapter(getActivity(), knjigiceAut);

            listaOdabranihKnjiga.setAdapter(mojAdapter);
        }

            listaOdabranihKnjiga.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapter, View view, int i, long l) {
                    int idKnjige = -1;
                    String imeKnjige = null;
                    if(KategorijeAkt.kat) {
                        knjigiceKat.get(i).setBoju(true);
                        for (int k = 0; k < KategorijeAkt.listaKnjiga.size(); k++) {
                            //if (KategorijeAkt.listaKnjiga.get(k).getImeAutora().equals(knjigiceKat.get(i).getImeAutora()) && KategorijeAkt.listaKnjiga.get(k).getNaziv().equals(knjigiceKat.get(i).getNaziv()) && KategorijeAkt.listaKnjiga.get(k).getNazivKategorije().equals(knjigiceKat.get(i).getNazivKategorije())) {
                            if (KategorijeAkt.listaKnjiga.get(k).getId().equals(knjigiceKat.get(i).getId())) {
                                KategorijeAkt.listaKnjiga.get(k).setBoju(true);
                                mojAdapter.notifyDataSetChanged();
                                idKnjige = Integer.parseInt(KategorijeAkt.listaKnjiga.get(k).getId());
                                imeKnjige = KategorijeAkt.listaKnjiga.get(k).getNaziv();
                                break;
                            }
                        }

                    }
                    else {
                        knjigiceAut.get(i).setBoju(true);
                        for (int k = 0; k < KategorijeAkt.listaKnjiga.size(); k++) {
                            //if (KategorijeAkt.listaKnjiga.get(k).getImeAutora().equals(knjigiceAut.get(i).getImeAutora()) && KategorijeAkt.listaKnjiga.get(k).getNaziv().equals(knjigiceAut.get(i).getNaziv()) && KategorijeAkt.listaKnjiga.get(k).getNazivKategorije().equals(knjigiceAut.get(i).getNazivKategorije())) {
                            if (KategorijeAkt.listaKnjiga.get(k).getId().equals(knjigiceAut.get(i).getId())) {
                                KategorijeAkt.listaKnjiga.get(k).setBoju(true);
                                mojAdapter.notifyDataSetChanged();
                                idKnjige = Integer.parseInt(KategorijeAkt.listaKnjiga.get(k).getId());
                                imeKnjige = KategorijeAkt.listaKnjiga.get(k).getNaziv();
                                break;
                            }
                        }
                    }
                    if(imeKnjige != null) {
                        ContentValues noveVrijednosti = new ContentValues();
                        noveVrijednosti.put(KNJIGA_PREGLEDANA, 1);
                        String where = KNJIGA_ID + "=" + idKnjige + " AND " + KNJIGA_NAZIV + "= '" + imeKnjige + "'";
                        SQLiteDatabase db = mojaBaza.getWritableDatabase();
                        db.update(DATABASE_TABLE2, noveVrijednosti, where, null);
                    }
                }
            });

        return vi2;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        povratakDugme = (Button)getView().findViewById(R.id.dPovratak);
        povratakDugme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = getFragmentManager();
                fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
        });
    }
}
