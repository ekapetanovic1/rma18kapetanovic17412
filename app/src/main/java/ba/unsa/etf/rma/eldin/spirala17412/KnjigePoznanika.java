package ba.unsa.etf.rma.eldin.spirala17412;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

public class KnjigePoznanika extends IntentService {
    public static final int STATUS_START = 0;
    public static final int STATUS_FINISH = 1;
    public static final int STATUS_ERROR = 2;
    ArrayList<Knjiga> listaKnjiga = new ArrayList<>();

    public KnjigePoznanika() { super(null); }
    public KnjigePoznanika(String name) {
        super(name);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final ResultReceiver receiver = intent.getParcelableExtra("receiver");
            final String userId = intent.getStringExtra("idKorisnika");
            Bundle mojBundle = new Bundle();

            receiver.send(STATUS_START, Bundle.EMPTY);

            String urlPomocna = "https://www.googleapis.com/books/v1/users/" + userId + "/bookshelves";
            try {
                URL url = new URL(urlPomocna);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                String rezultat = convertStreamToString(in);
                JSONObject jo = new JSONObject(rezultat);
                JSONArray items = jo.getJSONArray("items");
                ArrayList<String> idKnjiga = new ArrayList<>();
                for(int h=0; h<items.length(); h++) {
                    JSONObject bookshelves = items.getJSONObject(h);
                    idKnjiga.add(bookshelves.getString("id"));
                }

                for (int j = 0; j < idKnjiga.size(); j++) {
                    urlPomocna = "https://www.googleapis.com/books/v1/users/" + userId + "/bookshelves/" + idKnjiga.get(j) + "/volumes";

                    url = new URL(urlPomocna);
                    urlConnection = (HttpURLConnection) url.openConnection();
                    in = new BufferedInputStream(urlConnection.getInputStream());

                    rezultat = convertStreamToString(in);
                    jo = new JSONObject(rezultat);
                    items = jo.getJSONArray("items");

                    for (int i = 0; i < items.length(); i++) {
                        JSONObject book = items.getJSONObject(i);
                        String id;
                        if (book.has("id"))
                            id = book.getString("id");
                        else
                            id = null;
                        JSONObject volumeInfo;
                        if (book.has("volumeInfo"))
                            volumeInfo = book.getJSONObject("volumeInfo");
                        else
                            volumeInfo = null;
                        String naziv;
                        if (volumeInfo.has("title"))
                            naziv = volumeInfo.getString("title");
                        else
                            naziv = null;
                        ArrayList<Autor> autori = new ArrayList<>();
                        if (volumeInfo.has("authors")) {
                            JSONArray sviAutori = volumeInfo.getJSONArray("authors");
                            for (int k = 0; k < sviAutori.length(); k++)
                                autori.add(new Autor(sviAutori.getString(k), id));
                        } else
                            autori = null;
                        String opis;
                        if (volumeInfo.has("description"))
                            opis = volumeInfo.getString("description");
                        else
                            opis = null;
                        String datumObjavljivanja;
                        if (volumeInfo.has("publishedDate"))
                            datumObjavljivanja = volumeInfo.getString("publishedDate");
                        else
                            datumObjavljivanja = null;
                        URL slika;
                        if (volumeInfo.has("imageLinks")) {
                            JSONObject imageLinks = volumeInfo.getJSONObject("imageLinks");
                            if (imageLinks.has("thumbnail"))
                                slika = new URL(imageLinks.getString("thumbnail"));
                            else
                                slika = null;
                        } else
                            slika = null;
                        int brojStranica;
                        if (volumeInfo.has("pageCount"))
                            brojStranica = volumeInfo.getInt("pageCount");
                        else
                            brojStranica = 0;

                        listaKnjiga.add(new Knjiga(id, naziv, autori, opis, datumObjavljivanja, slika, brojStranica));
                    }
                }
                mojBundle.putParcelableArrayList("listaKnjiga", listaKnjiga);
                receiver.send(STATUS_FINISH, mojBundle);

            } catch (MalformedURLException e) {
                receiver.send(STATUS_ERROR, Bundle.EMPTY);
                e.printStackTrace();
            } catch (IOException r) {
                receiver.send(STATUS_ERROR, Bundle.EMPTY);
                r.printStackTrace();
            } catch (JSONException e) {
                receiver.send(STATUS_ERROR, Bundle.EMPTY);
                e.printStackTrace();
            }
        }
    }
    public String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while((line = reader.readLine()) != null)
                sb.append(line + "\n");
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
}
