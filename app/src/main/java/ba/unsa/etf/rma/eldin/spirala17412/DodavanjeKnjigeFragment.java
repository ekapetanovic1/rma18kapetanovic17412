package ba.unsa.etf.rma.eldin.spirala17412;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.IOException;

import static android.app.Activity.RESULT_OK;
import static ba.unsa.etf.rma.eldin.spirala17412.KategorijeAkt.mojaBaza;

public class DodavanjeKnjigeFragment extends Fragment {
    EditText ime;
    EditText naziv;
    Button nadjiSliku;
    Button upisiKnjigu;
    Button ponistiAkt;
    Spinner katSpiner;
    ImageView imageView;
    ArrayAdapter<String> adapterKategorije;

    public DodavanjeKnjigeFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_dodavanje_knjige_akt, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ime = (EditText)getView().findViewById(R.id.imeAutora);
        naziv = (EditText)getView().findViewById(R.id.nazivKnjige);
        nadjiSliku = (Button)getView().findViewById(R.id.dNadjiSliku);
        upisiKnjigu = (Button)getView().findViewById(R.id.dUpisiKnjigu);
        ponistiAkt = (Button)getView().findViewById(R.id.dPonisti);
        katSpiner = (Spinner)getView().findViewById(R.id.sKategorijaKnjige);
        imageView = (ImageView)getView().findViewById(R.id.naslovnaStr);
        imageView.setImageResource(R.mipmap.ic_launcher);

        adapterKategorije = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, KategorijeAkt.listaKat);
        katSpiner.setAdapter(adapterKategorije);

        upisiKnjigu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ime.getText().toString().equals(""))
                    Toast.makeText(getContext(), "Polje za unos imena autora je prazno!", Toast.LENGTH_SHORT).show();
                else if (naziv.getText().toString().equals(""))
                    Toast.makeText(getContext(), "Polje za unos naziva knjige je prazno!", Toast.LENGTH_SHORT).show();
                else if (KategorijeAkt.listaKat.size() == 0 || KategorijeAkt.listaKat == null)
                    Toast.makeText(getContext(), "Polje za izbor kategorije je prazno!", Toast.LENGTH_SHORT).show();
                else {
                    try {
                        if(mojaBaza.dodajKnjigu(new Knjiga(ime.getText().toString(), naziv.getText().toString(), katSpiner.getSelectedItem().toString(), false)) == -1)
                            throw new Exception("Pogreška pri dodavanju knjige u bazu podataka!");
                    }
                    catch (Exception e) {
                        Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                    KategorijeAkt.listaKnjiga.add(new Knjiga(ime.getText().toString(), naziv.getText().toString(), katSpiner.getSelectedItem().toString(), false));
                    ime.setText("");
                    naziv.setText("");
                    katSpiner.setSelection(0);
                    ime.requestFocus();
                    imageView.setImageResource(R.mipmap.ic_launcher);
                    Toast.makeText(getContext(), "Knjiga uspješno registrovana!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        ponistiAkt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(KategorijeAkt.siriL) {
                    FrameLayout fl = (FrameLayout) getActivity().findViewById(R.id.listeF);
                    fl.setLayoutParams(KategorijeAkt.parametar);
                }
                FragmentManager fm2 = getFragmentManager();
                fm2.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
        });

        nadjiSliku.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent slikaIntent = new Intent();
                slikaIntent.setType("image/*");
                slikaIntent.setAction(Intent.ACTION_GET_CONTENT);
                //slikaIntent.addCategory(Intent.CATEGORY_OPENABLE);
                startActivityForResult(slikaIntent, 1);
            }
        });
    }

    Bitmap getBitmapFromUri (Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor = getContext().getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return image;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EditText naziv2 = (EditText)getView().findViewById(R.id.nazivKnjige);
        imageView = (ImageView)getView().findViewById(R.id.naslovnaStr);

        if (requestCode == 1 && resultCode == RESULT_OK && data != null && data.getData() != null) {
            try {
                FileOutputStream outputStream;
                outputStream = getContext().openFileOutput(naziv2.getText().toString(), Context.MODE_PRIVATE);
                getBitmapFromUri(data.getData()).compress(Bitmap.CompressFormat.JPEG,90,outputStream);
                outputStream.close();

                imageView.setImageBitmap(BitmapFactory.decodeStream(getContext().openFileInput(naziv2.getText().toString())));
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
