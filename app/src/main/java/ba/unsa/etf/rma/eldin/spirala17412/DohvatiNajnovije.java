package ba.unsa.etf.rma.eldin.spirala17412;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

public class DohvatiNajnovije extends AsyncTask<String, Integer, Void> {
    ArrayList<Knjiga> knjige = new ArrayList<>();
    private IDohvatiNajnovijeDone pozivatelj;
    public DohvatiNajnovije(IDohvatiNajnovijeDone p) { pozivatelj = p; }

    @Override
    protected Void doInBackground(String... params) {
            String query = null;
            try {
                query = URLEncoder.encode(params[0], "utf-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            String urlPomocna = "https://www.googleapis.com/books/v1/volumes?q=inauthor:" + query + "&orderBy=newest&maxResults=5";
            try {
                URL url = new URL(urlPomocna);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                String rezultat = convertStreamToString(in);
                JSONObject jo = new JSONObject(rezultat);
                JSONArray items = jo.getJSONArray("items");

                for (int i = 0; i < items.length(); i++) {
                    JSONObject book = items.getJSONObject(i);
                    String id;
                    if (book.has("id"))
                        id = book.getString("id");
                    else
                        id = null;
                    JSONObject volumeInfo;
                    if (book.has("volumeInfo"))
                        volumeInfo = book.getJSONObject("volumeInfo");
                    else
                        volumeInfo = null;
                    String naziv;
                    if (volumeInfo.has("title"))
                        naziv = volumeInfo.getString("title");
                    else
                        naziv = null;
                    ArrayList<Autor> autori = new ArrayList<>();
                    if (volumeInfo.has("authors")) {
                        JSONArray sviAutori = volumeInfo.getJSONArray("authors");
                        for (int k = 0; k < sviAutori.length(); k++)
                            autori.add(new Autor(sviAutori.getString(k), id));
                    } else
                        autori = null;
                    String opis;
                    if (volumeInfo.has("description"))
                        opis = volumeInfo.getString("description");
                    else
                        opis = null;
                    String datumObjavljivanja;
                    if (volumeInfo.has("publishedDate"))
                        datumObjavljivanja = volumeInfo.getString("publishedDate");
                    else
                        datumObjavljivanja = null;
                    URL slika;
                    if (volumeInfo.has("imageLinks")) {
                        JSONObject imageLinks = volumeInfo.getJSONObject("imageLinks");
                        if (imageLinks.has("thumbnail"))
                            slika = new URL(imageLinks.getString("thumbnail"));
                        else
                            slika = null;
                    } else
                        slika = null;
                    int brojStranica;
                    if (volumeInfo.has("pageCount"))
                        brojStranica = volumeInfo.getInt("pageCount");
                    else
                        brojStranica = 0;

                    knjige.add(new Knjiga(id, naziv, autori, opis, datumObjavljivanja, slika, brojStranica));
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException r) {
                r.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        pozivatelj.onNajnovijeDone(knjige);
    }

    public interface IDohvatiNajnovijeDone {
        public void onNajnovijeDone(ArrayList<Knjiga> knjigice);
    }

    public String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while((line = reader.readLine()) != null)
                sb.append(line + "\n");
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
}
