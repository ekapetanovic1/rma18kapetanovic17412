package ba.unsa.etf.rma.eldin.spirala17412;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class ListaKnjigaAkt extends AppCompatActivity {
    ListView listaOdabranihKnjiga;
    Button povratakDugme;
    MyArrayAdapter mojAdapter;
    ArrayList<Knjiga> knjige = new ArrayList<>();
    ArrayList<String> kategorije = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_knjiga_akt);

        listaOdabranihKnjiga = (ListView)findViewById(R.id.listaKnjiga);
        povratakDugme = (Button)findViewById(R.id.dPovratak);

        Intent i = this.getIntent();
        /*if(!((ArrayList<Knjiga>)i.getSerializableExtra("izabraneKnjige") == null))
            knjige = (ArrayList<Knjiga>)i.getSerializableExtra("izabraneKnjige");*/

        kategorije = i.getStringArrayListExtra("izabraneKategorije");

        mojAdapter = new MyArrayAdapter(this, knjige);

        listaOdabranihKnjiga.setAdapter(mojAdapter);

        povratakDugme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent knjigiceIntent = new Intent(ListaKnjigaAkt.this, KategorijeAkt.class);
                knjigiceIntent.putStringArrayListExtra("nekeKategorije", kategorije);
                ListaKnjigaAkt.this.startActivity(knjigiceIntent);
            }
        });

        listaOdabranihKnjiga.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View view, int i, long l) {
                knjige.get(i).setBoju(true);
                for(int k=0; k < KategorijeAkt.listaKnjiga.size(); k++)
                    if(KategorijeAkt.listaKnjiga.get(k).getImeAutora().equals(knjige.get(i).getImeAutora()) && KategorijeAkt.listaKnjiga.get(k).getNaziv().equals(knjige.get(i).getNaziv()) && KategorijeAkt.listaKnjiga.get(k).getNazivKategorije().equals(knjige.get(i).getNazivKategorije()))
                        KategorijeAkt.listaKnjiga.get(k).setBoju(true);
                mojAdapter.notifyDataSetChanged();
            }
        });
    }

}
