package ba.unsa.etf.rma.eldin.spirala17412;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

import static ba.unsa.etf.rma.eldin.spirala17412.KategorijeAkt.mojaBaza;

public class FragmentOnline extends Fragment implements DohvatiKnjige.IDohvatiKnjigeDone, DohvatiNajnovije.IDohvatiNajnovijeDone, MojResultReceiver.Receiver{
    Spinner spinKategorije;
    EditText txtUpit;
    Button buttonPretraga;
    Spinner spinRezultat;
    Button buttonDodajKnjigu;
    Button buttonPovratak;
    ArrayAdapter<String> adapterKategorije;
    ArrayAdapter<String> adapterKnjige;
    ArrayList<String> imenaKnjiga = new ArrayList<>();
    MojResultReceiver mReceiver;
    ArrayList<Knjiga> knjige = new ArrayList<>();

    public FragmentOnline() {}

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.online_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        spinKategorije = (Spinner)getView().findViewById(R.id.sKategorije);
        txtUpit = (EditText)getView().findViewById(R.id.tekstUpit);
        buttonPretraga = (Button)getView().findViewById(R.id.dRun);
        spinRezultat = (Spinner)getView().findViewById(R.id.sRezultat);
        buttonDodajKnjigu = (Button)getView().findViewById(R.id.dAdd);
        buttonPovratak = (Button)getView().findViewById(R.id.dPovratak);

        adapterKategorije = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, KategorijeAkt.listaKat);
        spinKategorije.setAdapter(adapterKategorije);

        buttonPretraga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(txtUpit.getText().toString().contains(";")) {
                    String[] rijeci = txtUpit.getText().toString().split(";");
                    new DohvatiKnjige((DohvatiKnjige.IDohvatiKnjigeDone)FragmentOnline.this).execute(rijeci);
                }
                else if(txtUpit.getText().toString().contains("autor:")) {
                    String[] rijeci = txtUpit.getText().toString().split(":");
                    new DohvatiNajnovije((DohvatiNajnovije.IDohvatiNajnovijeDone)FragmentOnline.this).execute(rijeci[1]);
                }
                else if(txtUpit.getText().toString().contains("korisnik:")) {
                    String[] rijeci = txtUpit.getText().toString().split(":");
                    mReceiver = new MojResultReceiver(new Handler());
                    mReceiver.setReceiver(FragmentOnline.this);
                    Intent in = new Intent(Intent.ACTION_SYNC, null, getActivity(), KnjigePoznanika.class);
                    in.putExtra("idKorisnika", rijeci[1]);
                    in.putExtra("receiver", mReceiver);
                    getActivity().startService(in);
                }
                else {
                    new DohvatiKnjige((DohvatiKnjige.IDohvatiKnjigeDone)FragmentOnline.this).execute(txtUpit.getText().toString());
                }
            }
        });

        buttonDodajKnjigu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(spinKategorije.getCount() == 0)
                    Toast.makeText(getActivity(), "Spiner za izbor kategorije je prazan!", Toast.LENGTH_SHORT).show();
                else if(spinRezultat.getCount() == 0)
                    Toast.makeText(getActivity(), "Spiner za izbor knjige je prazan!", Toast.LENGTH_SHORT).show();
                else {
                    for(int i=0; i<knjige.size(); i++) {
                        if(knjige.get(i).getNaziv().equals(spinRezultat.getSelectedItem().toString())) {
                            knjige.get(i).setNazivKategorije(spinKategorije.getSelectedItem().toString());
                            try {
                                if(mojaBaza.dodajKnjigu(knjige.get(i)) == -1)
                                    throw new Exception("Pogreška pri dodavanju knjige u bazu podataka!");
                            }
                            catch (Exception e) {
                                Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
                            }
                            KategorijeAkt.listaKnjiga.add(knjige.get(i));
                            break;
                        }
                    }
                    Toast.makeText(getActivity(), "Knjiga uspješno registrovana!", Toast.LENGTH_SHORT).show();

                    spinKategorije.setSelection(0);
                    spinRezultat.setSelection(0);
                }
            }
        });

        buttonPovratak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(KategorijeAkt.siriL) {
                    FrameLayout fl = (FrameLayout) getActivity().findViewById(R.id.listeF);
                    fl.setLayoutParams(KategorijeAkt.parametar);
                }
                FragmentManager fm2 = getFragmentManager();
                fm2.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
        });
    }

    @Override
    public void onDohvatiDone(ArrayList<Knjiga> knjigice) {
        knjige = knjigice;
        imenaKnjiga.clear();
        for(int i=0; i<knjige.size(); i++)
            imenaKnjiga.add(knjige.get(i).getNaziv());

        adapterKnjige = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, imenaKnjiga);
        spinRezultat.setAdapter(adapterKnjige);
    }

    @Override
    public void onNajnovijeDone(ArrayList<Knjiga> knjigice) {
        knjige = knjigice;
        imenaKnjiga.clear();
        for(int i=0; i<knjige.size(); i++)
            imenaKnjiga.add(knjige.get(i).getNaziv());

        adapterKnjige = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, imenaKnjiga);
        spinRezultat.setAdapter(adapterKnjige);
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        switch (resultCode) {
            case KnjigePoznanika.STATUS_START:
                break;
            case KnjigePoznanika.STATUS_FINISH:
                knjige = resultData.getParcelableArrayList("listaKnjiga");
                imenaKnjiga.clear();
                for(int i=0; i<knjige.size(); i++)
                    imenaKnjiga.add(knjige.get(i).getNaziv());

                adapterKnjige = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, imenaKnjiga);
                spinRezultat.setAdapter(adapterKnjige);
                break;
            case KnjigePoznanika.STATUS_ERROR:
                Toast.makeText(getActivity(), "Došlo je do pogreške!", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
