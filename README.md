# Vođenje evidencije o knjigama


### Android aplikacija
Projekat urađen u Android Studio-u, korištena Java programski jezik.
Aplikacija je napravljena tako da korisnik vodi evidenciju o knjigama, pročitanim ili interesantnim za korisnika.
Knjige je moguće dodavati ručno, pomoću formi za unos podataka(naslov, ime autora, slika, kategorija i slično), također korištenjem Google-ovog API za knjige.
Moguće je izlistati knjige po kategorijama ili autorima. Kada se klikne na dugme koje predstavlja određenu kategoriju odnosno kategoriju, 
izlistavaju se sve knjige koje pripadaju odabranoj kategoriji odnosno sve knjige koje je napisao izabrani autor.
Knjige je moguće selektovati i poslati poruku preporuke prijateljima iz kontakata na mobilnom uređaju.
Stanje selektovanih knjiga se spašava u bazu podataka(MySQL), kako bi se prilikom idućeg otvaranja aplikacije, 
učitalo stanje kako smo i ostavili prilikom posljednjeg korištenja aplikacije.
Kategorije je moguće pretraživati ili dodavati, ukoliko već ne postoje u bazi podataka.